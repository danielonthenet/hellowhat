package main

import (
	"fmt"
	"os"
	"runtime"
	"time"	

	"github.com/nats-io/nats.go"
)

func main() {
	subject := os.Getenv("SUBJECT_NAME")
	natsURL := os.Getenv("NATS_SERVER_URL")

	opts := nats.Options{
		AllowReconnect: true,
		MaxReconnect:   5,
		ReconnectWait:  5 * time.Second,
		Timeout:        3 * time.Second,
		Url:            natsURL,
	}

	conn, _ := opts.Connect()
	
	fmt.Println("Publisher connected to NATS server")

	fmt.Printf("Publishing to subject %s\n", subject)

	for {
		msg := "Hello NATS "+ time.Now().String()
		msgBytes := []byte(msg)
		conn.Publish(subject, msgBytes)
		fmt.Printf("Sent message %s\n", msg)
		time.Sleep(5 * time.Second)
	}

	runtime.Goexit()
}
