package main

import (
	"fmt"
	"os"
	"runtime"
	"time"

	"github.com/nats-io/nats.go"
)

func main() {
	subject := os.Getenv("SUBJECT_NAME")
	natsURL := os.Getenv("NATS_SERVER_URL")

	opts := nats.Options{
		AllowReconnect: true,
		MaxReconnect:   5,
		ReconnectWait:  5 * time.Second,
		Timeout:        3 * time.Second,
		Url:            natsURL,
	}

	conn, _ := opts.Connect()
	
	fmt.Println("Subscriber connected to NATS server")

	fmt.Printf("Subscribing to subject %s\n", subject)
	conn.Subscribe(subject, func(msg *nats.Msg) {
		fmt.Printf("Got message '%s\n", string(msg.Data)+"'")
	})

	runtime.Goexit()
}
