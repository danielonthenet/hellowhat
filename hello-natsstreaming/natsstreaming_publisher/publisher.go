package main

import (
	"log"
	"os"
	"time"
	"fmt"

	nats "github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
)


func main() {
	clusterID := os.Getenv("CLUSTER_ID")
	clientID := os.Getenv("CLIENT_ID")
	URL := os.Getenv("NATS_SERVER_URL")
	subj := os.Getenv("SUBJECT_NAME")

	// Connect Options.
	opts := []nats.Option{nats.Name("NATS Streaming Example Publisher")}
	
	// Connect to NATS
	nc, err := nats.Connect(URL, opts...)

	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	sc, err := stan.Connect(clusterID, clientID, stan.NatsConn(nc))
	if err != nil {
		log.Fatalf("Can't connect: %v.\nMake sure a NATS Streaming Server is running at: %s", err, URL)
	}
	defer sc.Close()

	for {
		msg := []byte("Hello NATS Streaming"+ time.Now().String())

		err = sc.Publish(subj, msg)
		if err != nil {
			log.Fatalf("Error during publish: %v\n", err)
		}
		fmt.Printf("Sent message %s\n", msg)
		time.Sleep(5 * time.Second)
	}	
}
