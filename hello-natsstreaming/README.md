# NATS

NATS.io is a simple, secure and high performance open source messaging system for cloud native applications, IoT messaging, and microservices architectures.

Find more detail here: https://nats.io/

<HR>

## HelloNATS

### 1. Introduction

- HelloNATS has two compenents, a publisher and a subscriber, both written in GO. The publisher publishes messages to the NATS server on a topic. The receiver subscribes into the NATS server on the same topic. Each of the component logs to the console as and when performing an operation such as publishing or receiving.

### 2. Running

```
docker-compose build --no-cache

docker-compose up
```

### 3. Verifying

- The containers should be both running
- The publisher will log messages as it publishes new messages
- The subscriber will log upon receiving the messages
- The logs can be verified to ensure that the subscriber receives messages published by the publisher.

### 4. Cleanup

- Since `docker-compose` was not run in detached mode, just cancel (ctrl + C) to stop the containers
- `docker-compose down` to cleanup
